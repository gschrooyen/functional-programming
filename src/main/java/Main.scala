import javafx.application.{Application, Platform}
import javafx.event.EventHandler
import javafx.scene.image.Image
import javafx.scene.Scene
import javafx.stage.Stage
import model.AlphabetManager
import org.apache.logging.log4j.{LogManager, Logger}
import utilities.DataProcessor
import views.StartView


object Main
{
  def main(args: Array[String])
  {
    Application.launch(classOf[Main], args: _*)
  }
}

class Main extends Application
{
  override def start(primaryStage: Stage) : Unit =
  {
    val logger: Logger = LogManager.getLogger()
    val scene: Scene = new Scene(new StartView)
    val processor = new DataProcessor(new AlphabetManager)
    val t: Thread = new Thread(processor)
    t.start()
    scene.getStylesheets.add("style.css")
    primaryStage.setTitle("NGram Analyser")
    primaryStage.setMaximized(true)
    primaryStage.setScene(scene)
    primaryStage.getIcons.add(new Image("EMplus_100px.png"))
    logger.info("program started")
    primaryStage.setOnCloseRequest(e => {
      Platform.exit()
      System.exit(0)
    })
    primaryStage.show()

  }
}
