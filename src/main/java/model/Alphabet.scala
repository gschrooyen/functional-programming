package model

case class Alphabet(language: String, complete: String, vowels: String, consonants: String) {

  def getUniGrams: IndexedSeq[String] ={
      for (letter <- complete) yield letter.toString
  }

  def getBiGrams: IndexedSeq[String] = {
    for (letter <- complete; letter2 <- complete) yield { letter.toString + letter2.toString}

  }

  def getTriGrams: IndexedSeq[String] = {
    for (letter1 <- complete; letter2 <- complete; letter3 <- complete) yield {letter1.toString + letter2.toString + letter3.toString}
  }

  def getSkipgram: IndexedSeq[String] ={
      for (letter <- complete; letter2 <- complete) yield {letter.toString + "_" + letter2.toString}
  }

  def getLanguage: String ={
    language
  }

  override def toString: String = {
    language + "; " + complete
  }
}
