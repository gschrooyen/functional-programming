package model

class AlphabetManager {

  val Danish : Alphabet = Alphabet("Danish", "abcdefghijklmnopqrstuvwxyzæåø", "aeiouæåø", "bcdfghjklmnpqrstvwxyz")
  val Finnish = Alphabet("Finnish", "abcdefghijklmnopqrstuvwxyzäö", "aeiouäö", "bcdfghjklmnpqrstvwxyz")
  val Italian = Alphabet("Italian", "abcdefghilmnopqrstuvz", "aeiou", "bcdfghlmnpqrstvz")
  val English = Alphabet("English", "abcdefghijklmnopqrstuvwxyz", "aeiou", "bcdfghjklmnpqrstvwxyz")
  val Dutch = Alphabet("Dutch", "abcdefghijklmnopqrstuvwxyz" , "aeiou", "bcdfghjklmnpqrstvwxyz")
  val Portuguese = Alphabet("Portuguese", "abcdefghijklmnopqrstuvwxyz", "aeiou", "bcdfghjklmnpqrstvwxyz")
  val German = Alphabet("German", "abcdefghijklmnopqrstuvwxyzäöüß", "aeiouäöü", "bcdfghjklmnpqrstvwxyzß")
  val Spanish = Alphabet("Spanish", "abcdefghijklmnñopqrstuvwxyz", "aeiou", "bcdfghjklmnñpqrstvwxyz")


  def getAlphabet(language:String): Alphabet ={
      language.toUpperCase match {
        case "DANISH" => Danish
        case "FINNISH" => Finnish
        case "ITALIAN" => Italian
        case "DUTCH" => Dutch
        case "ENGLISH" => English
        case "PORTUGUESE" => Portuguese
        case "GERMAN" => German
        case "SPANISH" => Spanish
        case _ => Alphabet(null, null, null, null)
      }
  }

  def getAllAlphabets(): Vector[Alphabet]  ={
    this.getClass.getDeclaredFields.map(f => f.get(this)).toVector.asInstanceOf[Vector[Alphabet]]
  }
}
