package model


import scala.collection.immutable.{HashSet, ListMap}
import scala.language.postfixOps

class NGramsAnalyser {

  def getBeginsWith(alphabet: Alphabet, woorden: Vector[String]): Map[String, Double] = {
    val totaal = woorden.size.toDouble
    val set = HashSet.from(alphabet.complete)
    val wrdn = toLowercase(woorden).flatMap(w => w.substring(0, 1))
    val test = wrdn.groupMapReduce(k => {
      if (set.contains(k)) {
        k
      } else {
        "other"
      }
    }.toString)(k => 1 / totaal)(_ + _)
    test.removed("other")
  }

  def getEndsWith(alphabet: Alphabet, woorden: Vector[String]): Map[String, Double] = {
    val totaal = woorden.size.toDouble
    val set = HashSet.from(alphabet.complete)
    val wrdn = toLowercase(woorden).flatMap(w => w.split("").last)
    val test = wrdn.groupMapReduce(k => {
      if (set.contains(k)) {
        k
      } else {
        "other"
      }
    }.toString)(_ => 1 / totaal)(_ + _)
    test.removed("other")
  }

  def getOccurence(alphabet: Alphabet, woorden: Vector[String]): Map[String, Double] = {
    val langestring = toLowercase(woorden).mkString("")
    val mySet = HashSet.from(alphabet.complete)
    val totaal = langestring.replace(" ", "").length.toDouble
    val test = toLowercase(woorden).flatMap(w => w.toCharArray).groupMapReduce(k => {
      if (mySet.contains(k)) {
        k
      } else {
        "other"
      }
    }.toString)(k => 1 / totaal)(_ + _)
    test.removed("other")
  }

  def getBeginsWith(strlist: Vector[String], woorden: Vector[String]): Map[String, Double] = {
    val wrdn = toLowercase(woorden)
    val set = HashSet.from(strlist)
    val totaal = woorden.length.toDouble
    wrdn.groupMapReduce(k => {
      if (k.length >= set.last.length) {
        if (set.contains(k.substring(0, set.last.length))) {
          k.substring(0, set.last.length)
        } else {
          "other"
        }
      }else{
        "other"
      }
    })(k => 1 / totaal)(_ + _).removed("other")

  }

  def getEndsWith(strlist: Vector[String], woorden: Vector[String]): Map[String, Double] = {
    val totaal = woorden.length.toDouble
    val wrdn = toLowercase(woorden)
    val set = HashSet.from(strlist)
    wrdn.groupMapReduce(k => {
      if (set.contains({
        if (k.length >= set.last.length) {
          k.substring(k.length - set.last.length)
        } else {
          k
        }
      })) {
        k.substring(k.length - set.last.length)
      } else {
        "other"
      }
    })(_ => 1 / totaal)(_ + _).removed("other")
  }

  def getOccurence(strlist: Vector[String], woorden: Vector[String]): Map[String, Double] = {
    val langestring = toLowercase(woorden).flatMap(w => w.sliding(strlist.last.length))
    val totaal = langestring.length.toDouble
    val mySet = HashSet.from(strlist)
    val test = langestring.groupMapReduce(k => {
      if (mySet.contains(k)) {
        k
      } else {
        "other"
      }
    })(_ => 1 / totaal)(_ + _)
    test.removed("other")
  }

  def vowAndCons(alphabet: Alphabet, woorden: Vector[String]): Map[String, Double] = {
    val list = toLowercase(woorden).flatMap(w => w.split(""))
    val totaal = list.length.toDouble
    val vset = HashSet.from(alphabet.vowels)
    val cset = HashSet.from(alphabet.consonants)
    val vandc = list.groupMapReduce(k => {
      if (cset.contains(k.charAt(0))) {
        "vowels"
      } else if (vset.contains(k.charAt(0))) {
        "consonants"
      } else {
        "other"
      }
    }.toString)(k => 1 / totaal)(_ + _)
    vandc.removed("other")
  }

  def getSkipOccurrence(alphabet: Alphabet, woorden: Vector[String]): Map[String, Double] = {
    val skipgrams = HashSet.from(alphabet.getSkipgram)
    val langestring = toLowercase(woorden).flatMap(w => w.sliding(3))
    val totaal = langestring.length.toDouble
    val result = langestring.groupMapReduce(k => {
      if (k.length == 3) {
        if (skipgrams.contains(k.substring(0, 1) + "_" + k.substring(2, 3))) {
          k.substring(0, 1) + "_" + k.substring(2, 3)
        } else {
          "other"
        }
      } else {
        "other"
      }
    })(_ => 1 / totaal)(_ + _).removed("other")
    val sorted = ListMap(result.toSeq.sortBy(_._2 * -1): _*)
    sorted.take(25)
  }

  def beginsWithBigrams(alphabet: Alphabet, woorden: Vector[String]): Map[String, Double] = {
    val all = getBeginsWith(alphabet.getBiGrams.toVector, toLowercase(woorden))
    ListMap(all.toSeq.sortBy(_._2 * -1): _*).take(25)
  }

  def endsWithBigrams(alphabet: Alphabet, woorden: Vector[String]): Map[String, Double] = {
    val all = getEndsWith(alphabet.getBiGrams.toVector, toLowercase(woorden))
    ListMap(all.toSeq.sortBy(_._2 * -1): _*).take(25)
  }

  def getBiAndTriOccurrence(alphabet: Alphabet, woorden: Vector[String]): Map[String, Double] = {
    val allBi = getOccurence(alphabet.getBiGrams.toVector, toLowercase(woorden))
    val bestBi = ListMap(allBi.toSeq.sortBy(_._2 * -1): _*).take(25)
    val allTri = getOccurence(alphabet.getTriGrams.toVector, toLowercase(woorden))
    val bestTri = ListMap(allTri.toSeq.sortBy(_._2 * -1): _*).take(25)
    bestBi ++ bestTri
  }

  def getBigramForSkipgram(alphabet: Alphabet, woorden: Vector[String]): Map[String, Double] = {
    val bigrams = getOccurence(alphabet.getBiGrams.toVector, toLowercase(woorden))
    val skipgrams = getSkipOccurrence(alphabet, toLowercase(woorden))
    val vect = for (key <- skipgrams.keys.toVector) yield (key + " => " +key.replace("_", ""), bigrams.getOrElse(key.replace("_", ""), 0.0))
    vect.toMap
  }

  def toLowercase(woorden: Vector[String]): Vector[String] = {
    woorden.map(w => w.toLowerCase())
  }
}
