package presenter

import java.time.LocalDateTime
import java.{util => ju}

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import model.{AlphabetManager, NGramsAnalyser}
import org.apache.logging.log4j.{LogManager, Logger}
import utilities.IOManager

import scala.collection.JavaConverters._
import scala.collection.mutable

class NGramsPresenter() {
  val logger: Logger = LogManager.getLogger()
  val alphabetManager: AlphabetManager = new AlphabetManager
  val analyser = new NGramsAnalyser
  val ioManager = new IOManager

  def getOccurrence(lang: String): Map[String, Double] = {
    logInfo("getOccurrence", lang)
    try {
      val map = getDataFromJson("processing/occurrence_" + lang + ".json")
      logger.info("Getting data from preprocessed file")
      map.toMap
    }
    catch {
      case _: Exception => {
        logger.warn("Could not find preprocessed file. Shifting to analyser-method ")
        val vector = ioManager.getCleanedText(lang)
        analyser.getOccurence(alphabetManager.getAlphabet(lang), vector)
      }
    }
  }

  def getBeginsWith(lang: String) = {
    logInfo("getBeginsWith", lang)

    try {
      val map = getDataFromJson("processing/beginsWith_" + lang + ".json")
      logger.info("Getting data from preprocessed file")
      map.toMap
    }
    catch {
      case _ : Exception =>
        logger.warn("Could not find preprocessed file. Shifting to analyser-method ")
        val vector = ioManager.getCleanedText(lang)
        analyser.getBeginsWith(alphabetManager.getAlphabet(lang), vector)
    }
  }

  def getEndsWith(lang: String): Map[String, Double] = {
    logInfo("getEndsWith", lang)

    try{
      val map = getDataFromJson("processing/endsWith_" + lang + ".json")
      logger.info("Getting data from preprocessed file")
      map.toMap
    }
    catch {
      case _: Exception => {
        logger.warn("Could not find preprocessed file. Shifting to analyser-method ")
        val vector = ioManager.getCleanedText(lang)
        analyser.getEndsWith(alphabetManager.getAlphabet(lang), vector)
      }
    }
  }

  def vowsAndCons(lang: String): Map[String, Double] = {
    logInfo("vowsAndCons", lang)

    try{
      val map = getDataFromJson("processing/vandc_" + lang + ".json")
      logger.info("Getting data from preprocessed file")
      map.toMap
    }
    catch {
      case _: Exception => {
        logger.warn("Could not find preprocessed file. Shifting to analyser-method ")
        val vector = ioManager.getCleanedText(lang)
        analyser.vowAndCons(alphabetManager.getAlphabet(lang), vector)
      }
    }
  }

  def beginsWithBigrams(lang: String): Map[String, Double] = {
    logInfo("beginsWithBigrams", lang)

    try{
      val map = getDataFromJson("processing/beginsWithBi_" + lang + ".json")
      logger.info("Getting data from preprocessed file")
      map.toMap
    }
    catch {
      case _: Exception => {
        logger.warn("Could not find preprocessed file. Shifting to analyser-method ")
        val vector = ioManager.getCleanedText(lang)
        try {
          analyser.beginsWithBigrams(alphabetManager.getAlphabet(lang), vector)
        }
      }
    }
  }

  def endsWithBigrams(lang: String): Map[String, Double] = {
    logInfo("endsWithBigrams", lang )

    try{
      val map = getDataFromJson("processing/endsWithBi_" + lang + ".json")
      logger.info("Getting data from preprocessed file")
      map.toMap
    }
    catch {
      case _: Exception => {
        logger.warn("Could not find preprocessed file. Shifting to analyser-method ")
        val vector = ioManager.getCleanedText(lang)
        analyser.endsWithBigrams(alphabetManager.getAlphabet(lang), vector)
      }
    }
  }

  def getBiAndTriOccurrence(lang: String): Map[String, Double] = {
    logInfo("getBiAndTriOccurrence", lang)

    try {
      val map = getDataFromJson("processing/biTriOcc_" + lang + ".json")
      logger.info("Getting data from preprocessed file")
      map.toMap
    }
    catch {
      case _: Exception => {
        logger.warn("Could not find preprocessed file. Shifting to analyser-method ")
        val vector = ioManager.getCleanedText(lang)
        analyser.getBiAndTriOccurrence(alphabetManager.getAlphabet(lang), vector)
      }
    }
  }

  def getSkipOccurrence(lang: String): Map[String, Double] = {
    logInfo("getSkipOccurrence", lang)

    try{
      val map = getDataFromJson("processing/skipOcc_" + lang + ".json")
      logger.info("Getting data from preprocessed file")
      map.toMap
    }
    catch {
      case _: Exception => {
        logger.warn("Could not find preprocessed file. Shifting to analyser-method ")
        val vector = ioManager.getCleanedText(lang)
        analyser.getSkipOccurrence(alphabetManager.getAlphabet(lang), vector)
      }
    }
  }

  def getBiSkip(lang: String): Map[String, Double] = {
    logInfo("getBiSkip", lang)
    try{
      val map = getDataFromJson("processing/biSkip_" + lang + ".json")
      logger.info("Getting data from preprocessed file")
      map.toMap
      }
    catch {
      case _: Exception => {
        logger.warn("Could not find preprocessed file. Shifting to analyser-method ")
        val vector = ioManager.getCleanedText(lang)
        analyser.getBigramForSkipgram(alphabetManager.getAlphabet(lang), vector)
      }
    }
  }

  def logInfo( method: String, lang: String) : Unit = {
    logger.info("Analysis requested: " + method + " for language: " + lang + " on: " + LocalDateTime.now())
  }

  def getDataFromJson(jsonStr: String) : mutable.Map[String, Double] = {
    val gson = new Gson
    val jsonString = scala.io.Source.fromFile(jsonStr).getLines.mkString
    val mapType = new TypeToken[ju.HashMap[String, Double]] {}.getType
    val map : mutable.Map[String, Double] = gson.fromJson[ju.Map[String, Double]](jsonString, mapType).asScala
    map
  }


}



