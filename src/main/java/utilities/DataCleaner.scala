package utilities

import java.text.Normalizer

class DataCleaner {
  def Cleanup(text: String): String ={
    text.replaceAll("[^\\p{L}\\ ]+|º", "")
      .replaceAll("\\ +", " ")
  }
}
