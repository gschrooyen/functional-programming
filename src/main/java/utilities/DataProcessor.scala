package utilities

import java.io.{BufferedWriter, File, FileWriter}

import com.google.gson.Gson
import model.{AlphabetManager, NGramsAnalyser}
import org.apache.logging.log4j.{LogManager, Logger}

import scala.collection.JavaConverters._

class DataProcessor(alphabets: AlphabetManager) extends Runnable {
  val LOGGER: Logger = LogManager.getLogger

  override def run(): Unit = {
    val path: String = "processing/"
    val dir = new File(path)
    dir.mkdir()
    val analyser = new NGramsAnalyser()
    val alphas = alphabets.getAllAlphabets()
    val IOManager: IOManager = new IOManager()

    alphas.foreach(alphabet => {

      val text = IOManager.getCleanedText(alphabet.getLanguage)

      LOGGER.info("start background processing for " + alphabet.getLanguage)


        val vandcJson =  toJson(analyser.vowAndCons(alphabet, text))
        val file = new File(path +"vandc_" + alphabet.getLanguage + ".json")
        val bw = new BufferedWriter(new FileWriter(file))
        bw.write(vandcJson)
        bw.close()


        val occurenceJson = toJson(analyser.getOccurence(alphabet, text))
        val fileocc = new File(path +"occurrence_" + alphabet.getLanguage + ".json")
        val bwocc = new BufferedWriter(new FileWriter(fileocc))
        bwocc.write(occurenceJson)
        bwocc.close()


        val beginsWithJson = toJson(analyser.getBeginsWith(alphabet, text))
        val filebegins = new File(path + "beginsWith_" + alphabet.getLanguage + ".json")
        val bwbegins = new BufferedWriter(new FileWriter(filebegins))
        bwbegins.write(beginsWithJson)
        bwbegins.close()



        val endswithJson = toJson(analyser.getEndsWith(alphabet, text))
        val fileends = new File(path + "endsWith_" + alphabet.getLanguage + ".json")
        val bwends = new BufferedWriter(new FileWriter(fileends))
        bwends.write(endswithJson)
        bwends.close()



        val beginswithBiJson = toJson(analyser.beginsWithBigrams(alphabet, text))
        val filebeginsbi = new File(path + "beginsWithBi_" + alphabet.getLanguage + ".json")
        val bwbeginsbi = new BufferedWriter(new FileWriter(filebeginsbi))
        bwbeginsbi.write(beginswithBiJson)
        bwbeginsbi.close()



        val endswithbiJson = toJson(analyser.endsWithBigrams(alphabet, text))
        val fileendsbi = new File(path + "endsWithBi_" + alphabet.getLanguage + ".json")
        val bwendsbi = new BufferedWriter(new FileWriter(fileendsbi))
        bwendsbi.write(endswithbiJson)
        bwendsbi.close()



        val biTriOccJson = toJson(analyser.getBiAndTriOccurrence(alphabet, text))
        val filebitri = new File(path + "biTriOcc_" + alphabet.getLanguage + ".json")
        val bwbitri = new BufferedWriter(new FileWriter(filebitri))
        bwbitri.write(biTriOccJson)
        bwbitri.close()
        LOGGER.info("done bi and tri")



        val skipoccJson = toJson(analyser.getSkipOccurrence(alphabet, text))
        val fileskip = new File(path + "skipOcc_" + alphabet.getLanguage + ".json")
        val bwskip = new BufferedWriter(new FileWriter(fileskip))
        bwskip.write(skipoccJson)
        bwskip.close()



        val data = analyser.getBigramForSkipgram(alphabet, text)
        val biskipJson = new Gson().toJson(data.asJava)
        val filebiskip = new File(path + "biSkip_" + alphabet.getLanguage + ".json")
        val bwbiskip = new BufferedWriter(new FileWriter(filebiskip))
        bwbiskip.write(biskipJson)
        bwbiskip.close()

      LOGGER.info("done processing " + alphabet.getLanguage)
    })
  }

  def toJson(data : Map[String, Double]): String = {
    val json_string = new Gson().toJson(data.asJava)
    json_string
  }

}
