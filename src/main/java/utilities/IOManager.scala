package utilities

import scala.io.Source

class IOManager
{
  def removePunctuation(vector: Vector[String]) = {
      vector.map(word => word.filter(Character.isLetter))
  }

  val cleaner = new DataCleaner
  val metaDataManager = new MetaDataManager

  def getCleanedText(name: String): Vector[String] ={
    val textSource = Source.fromFile("src/main/data/" + name + ".txt")
    val text = textSource.mkString
    val t: Thread = new Thread(() => {metaDataManager.generateMetaData(text.split(" ").toVector, name)})
    textSource.close()
    val output = cleaner.Cleanup(text).split(" ").toVector.filter(s => !"".equals(s))//.reduce((a,b) => a.concat(' ' + b)).split(' ').toVector.map(s => s.replace(" ", ""))

    t.start()
    output
  }

}
