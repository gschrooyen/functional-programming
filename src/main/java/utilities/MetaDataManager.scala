package utilities

import org.apache.logging.log4j.{Level, LogManager, Logger}

/**
 * @author Griet Vermeesch
 * @version 1.0 23/10/2019 11:10
 *
 */
class MetaDataManager {
  val logger: Logger = LogManager.getLogger

  def generateMetaData(vector: Vector[String], lang : String): Unit = {
    countUniqueWords(vector, lang)
    countPunctuations(vector, lang)
    countUpperCaseChars(vector, lang)
    countLowerCaseChars(vector, lang)
  }

  def countUniqueWords(vector: Vector[String], taal : String): Unit = {
    logger.log(Level.forName("META", 50), taal + " : " + vector.size + " woorden, maar " + vector.distinct.size + " unieke")
  }

  def countPunctuations(vector: Vector[String], taal : String) : Unit = {
    logger.log(Level.forName("META", 50), taal + " : " + vector.map(word => word.filter(!Character.isLetter(_))).distinct.size + " leestekens")
  }

  def countUpperCaseChars (vector: Vector[String], taal : String): Unit= {
    logger.log(Level.forName("META", 50), taal + " : " + vector.map(word => word.filter(c => Character.isUpperCase(c)).length).reduce((acc, cur) => acc+cur) + " uppercase karakters")
  }

  def countLowerCaseChars (vector: Vector[String], taal : String): Unit= {
    logger.log(Level.forName("META", 50), taal + " : " + vector.map(word => word.filter(c => Character.isLowerCase(c)).length).reduce((acc, cur) => acc+cur) + " lowercase karakters")
  }

}
