package views

import java.util.Comparator

import javafx.application.Platform
import javafx.beans.value.{ChangeListener, ObservableValue}
import javafx.geometry.Pos
import javafx.scene.chart._
import javafx.scene.control._
import javafx.scene.layout.{AnchorPane, BorderPane, HBox, VBox}
import org.apache.logging.log4j.{LogManager, Logger}
import presenter.NGramsPresenter

import scala.collection.immutable.ListMap


class BarGraphView(data: Map[String, Double], lang: String, analysis: String, ordered: Boolean) extends BorderPane {

  val presenter = new NGramsPresenter
  val LOGGER: Logger = LogManager.getLogger()

  val title = new Label("Welcome to our NGrams Analyser")
  title.getStyleClass.add("title")
  val homebtn = new Button("Home")
  homebtn.getStyleClass.add("homebtn")
  val homebox = new HBox(homebtn)
  homebox.getStyleClass.add("radio")


  val welcometext = new Label("Analyse up to eight languages as they are used in a number of texts from the Europarl Corpus.")
  val choosetext = new Label("Select another language, push the button, and compare.")
  val hometext = new Label("Push the home button to select another type of analysis")
  choosetext.getStyleClass.add("welcome")
  welcometext.getStyleClass.add("welcome")
  hometext.getStyleClass.add("welcome")
  val top = new VBox(homebox, title, welcometext, hometext)
  top.setPrefHeight(200)
  top.getStyleClass.add("top")

  val disclaimer = new Label("Made by Glenn and Griet")
  disclaimer.getStyleClass.add("disclaimer")
  val orderbtn = new RadioButton("Order data alphabetically")
  orderbtn.setSelected(ordered)
  orderbtn.getStyleClass.add("homebtn")
  val bottom = new VBox(orderbtn, disclaimer)
  bottom.setPrefHeight(50)

  this.getStyleClass.add("pane")

  val languageChoice = new ToggleGroup
  val danish = new RadioButton("Danish")
  danish.setToggleGroup(languageChoice)
  danish.getStyleClass.add("radio")
  val dutch = new RadioButton("Dutch")
  dutch.setToggleGroup(languageChoice)
  dutch.setSelected(true)
  dutch.getStyleClass.add("radio")
  val english = new RadioButton("English")
  english.setToggleGroup(languageChoice)
  english.getStyleClass.add("radio")
  val finnish = new RadioButton("Finnish")
  finnish.setToggleGroup(languageChoice)
  finnish.getStyleClass.add("radio")
  val german = new RadioButton("German")
  german.setToggleGroup(languageChoice)
  german.getStyleClass.add("radio")
  val italian = new RadioButton("Italian")
  italian.setToggleGroup(languageChoice)
  italian.getStyleClass.add("radio")
  val portuguese = new RadioButton("Portuguese")
  portuguese.setToggleGroup(languageChoice)
  portuguese.getStyleClass.add("radio")
  val spanish = new RadioButton("Spanish")
  spanish.setToggleGroup(languageChoice)
  spanish.getStyleClass.add("radio")

  languageChoice.getToggles.forEach(t => {
    if (t.asInstanceOf[RadioButton].getText.equals(lang)) {
      t.setSelected(true)
    }
  })


  languageChoice.selectedToggleProperty().addListener(new ChangeListener[Toggle] {
    override def changed(observable: ObservableValue[_ <: Toggle], oldValue: Toggle, newValue: Toggle): Unit = {
      val new_lang = newValue.asInstanceOf[RadioButton].getText
      refreshChart(new_lang)
    }
  })


  val left = new VBox()
  left.getStyleClass.add("vbox")

  left.getChildren.add(danish)
  left.getChildren.add(dutch)
  left.getChildren.add(english)
  left.getChildren.add(finnish)
  left.getChildren.add(german)
  left.getChildren.add(italian)
  left.getChildren.add(portuguese)
  left.getChildren.add(spanish)


  homebtn.setOnAction(_ => {
    changeSceneToHome()
  })

  orderbtn.setOnAction(_ => {
    refreshChart(lang)
  })

  def changeSceneToHome(): Unit = {
    setCenter(new ProgressIndicator())
    val thread = new Thread {
      override def run() {
        val view = new StartView()
        Platform.runLater(() => getScene.setRoot(view))
      }
    }
    thread.start()
  }

  top.setAlignment(Pos.CENTER)
  left.setAlignment(Pos.TOP_LEFT)
  bottom.setAlignment(Pos.BOTTOM_CENTER)

  setLeft(left)
  setTop(top)
  setBottom(bottom)


  if (!analysis.equals("bi_tri")) {
    val chart: BarChart[String, Number] = setBarGraphData(data)
    chart.getStyleClass.add("chartTitle")
    analysis match {
      case "occurrence" => chart.setTitle("The occurrence of each letter")
      case "begin" => chart.setTitle("Number of words beginning with letter")
      case "end" => chart.setTitle("Number of words ending with letter")
      case "bigram_begin" => chart.setTitle("Number of words beginning with bigram")
      case "bigram_end" => chart.setTitle("Number of words ending with bigram")
      case "skip" => chart.setTitle("25 most occurring skipgrams")
      case "bi_skip" => chart.setTitle("Bigrams corresponding to most occurring skipgrams")
    }
    setCenter(chart)
  }
  else {
    val chart1: BarChart[String, Number] = setBarGraphData(getBiData(data))
    chart1.setTitle("The 25 most occurring bigrams")
    val chart2: BarChart[String, Number] = setBarGraphData(getTriData(data))
    chart2.setTitle("The 25 most occurring trigrams")
    val anchorpane = new AnchorPane()

    AnchorPane.setTopAnchor(chart1, 10.0)
    AnchorPane.setLeftAnchor(chart1, 0.0)
    AnchorPane.setRightAnchor(chart1, 660.0)

    AnchorPane.setTopAnchor(chart2, 10.0)
    AnchorPane.setLeftAnchor(chart2, 600.0)
    AnchorPane.setRightAnchor(chart2, 20.0)

    anchorpane.getChildren.addAll(chart1, chart2)

    setCenter(anchorpane)
  }


  def orderData(data: Map[String, Double]): Map[String, Double] = {
    ListMap(data.toSeq.sortBy(_._1): _*)
  }


  def setBarGraphData(data: Map[String, Double]): BarChart[String, Number] = {
    val ordered = orderbtn.isSelected
    val xAxis = new CategoryAxis
    xAxis.setLabel("letter")
    val yAxis = new NumberAxis()
    yAxis.setLabel("percentage")
    val barGraph: BarChart[String, Number] = new BarChart(xAxis, yAxis)
    val dataSeries1 = new XYChart.Series[String, Number]

    if (ordered){
      val orderedData = orderData(data)
      for (key <- orderedData){
        dataSeries1.getData.add(new XYChart.Data[String, Number](key._1, key._2 * 100))
      }
    }
    else{
      for (key <- data) {
        dataSeries1.getData.add(new XYChart.Data[String, Number](key._1, key._2 * 100))
      }
      dataSeries1.getData.removeIf(d => d.getYValue.doubleValue() == 0.0)

      dataSeries1.getData.sort(Comparator.comparingDouble(d => d.getYValue.doubleValue() * -1))
    }

    barGraph.getData.add(dataSeries1)
    barGraph.setLegendVisible(false)
    barGraph
  }

  def getBiData(data: Map[String, Double]): Map[String, Double] = {
    data.filter(k => k._1.length == 2)
  }

  def getTriData(data: Map[String, Double]): Map[String, Double] = {
    data.filter(k => k._1.length == 3)
  }

  def refreshChart(lang: String): Unit = {
    setCenter(new ProgressIndicator())
    changeChart(lang)
  }


  def changeChart(lang: String): Unit = {
    val thread = new Thread {
      override def run() {
        val lang_new = languageChoice.getSelectedToggle.asInstanceOf[RadioButton].getText
        val orderData = orderbtn.isSelected
        analysis match {
          case "occurrence" =>
            val data = presenter.getOccurrence(lang)
            val view = new BarGraphView(data, lang_new, "occurrence", orderData)
            Platform.runLater(() => getScene.setRoot(view))
          case "begin" =>
            val data = presenter.getBeginsWith(lang)
            val view = new BarGraphView(data, lang_new, "begin", orderData)
            Platform.runLater(() => getScene.setRoot(view))
          case "end" =>
            val data = presenter.getEndsWith(lang)
            val view = new BarGraphView(data, lang_new, "end", orderData)
            Platform.runLater(() => getScene.setRoot(view))
          case "bigram_begin" =>
            val data = presenter.beginsWithBigrams(lang)
            val view = new BarGraphView(data, lang_new, "bigram_begin", orderData)
            Platform.runLater(() => getScene.setRoot(view))
          case "bigram_end" =>
            val data = presenter.endsWithBigrams(lang)
            val view = new BarGraphView(data, lang_new, "bigram_end", orderData)
            Platform.runLater(() => getScene.setRoot(view))
          case "bi_tri" =>
            val data = presenter.getBiAndTriOccurrence(lang)
            val view = new BarGraphView(data, lang_new, "bi_tri", orderData)
            Platform.runLater(() => getScene.setRoot(view))
          case "skip" =>
            val data = presenter.getSkipOccurrence(lang)
            val view = new BarGraphView(data, lang_new, "skip", orderData)
            Platform.runLater(() => getScene.setRoot(view))
          case "bi_skip" =>
            val data = presenter.getBiSkip(lang)
            val view = new BarGraphView(data, lang, "bi_skip", orderData)
            Platform.runLater(() => getScene.setRoot(view))
        }
      }
    }
    thread.start()
  }
}