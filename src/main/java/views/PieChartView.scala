package views

import javafx.application.Platform
import javafx.beans.value.{ChangeListener, ObservableValue}
import javafx.geometry.Pos
import javafx.scene.chart.PieChart
import javafx.scene.control.{Button, Label, ProgressIndicator, RadioButton, Toggle, ToggleGroup}
import javafx.scene.layout.{BorderPane, HBox, VBox}
import org.apache.logging.log4j.{LogManager, Logger}
import presenter.NGramsPresenter

class PieChartView (data: Map[String, Double], lang : String) extends BorderPane {

  val presenter = new NGramsPresenter
  val LOGGER: Logger = LogManager.getLogger()

  val title = new Label("Welcome to our NGrams Analyser")
  title.getStyleClass.add("title")
  val homebtn = new Button("Home")
  homebtn.getStyleClass.add("homebtn")
  val homebox = new HBox(homebtn)
  homebox.getStyleClass.add("radio")

  val welcometext = new Label("Analyse up to eight languages as they are used in a number of texts from the Europarl Corpus.")
  val choosetext = new Label("Select another language, push the button, and compare.")
  val hometext = new Label("Push the home button to select another type of analysis")
  choosetext.getStyleClass.add("welcome")
  welcometext.getStyleClass.add("welcome")
  hometext.getStyleClass.add("welcome")
  val top = new VBox(homebox, title, welcometext, hometext)
  top.setPrefHeight(200)
  top.getStyleClass.add("top")

  val disclaimer = new Label("Made by Glenn and Griet")
  disclaimer.getStyleClass.add("disclaimer")
  val bottom = new VBox(disclaimer)
  bottom.setPrefHeight(50)

  this.getStyleClass.add("pane")

  val languageChoice = new ToggleGroup
  val danish = new RadioButton("Danish")
  danish.setToggleGroup(languageChoice)
  danish.getStyleClass.add("radio")
  val dutch = new RadioButton("Dutch")
  dutch.setToggleGroup(languageChoice)
  dutch.getStyleClass.add("radio")
  val english = new RadioButton("English")
  english.setToggleGroup(languageChoice)
  english.getStyleClass.add("radio")
  val finnish = new RadioButton("Finnish")
  finnish.setToggleGroup(languageChoice)
  finnish.getStyleClass.add("radio")
  val german = new RadioButton("German")
  german.setToggleGroup(languageChoice)
  german.getStyleClass.add("radio")
  val italian = new RadioButton("Italian")
  italian.setToggleGroup(languageChoice)
  italian.getStyleClass.add("radio")
  val portuguese = new RadioButton("Portuguese")
  portuguese.setToggleGroup(languageChoice)
  portuguese.getStyleClass.add("radio")
  val spanish = new RadioButton("Spanish")
  spanish.setToggleGroup(languageChoice)
  spanish.getStyleClass.add("radio")


  languageChoice.getToggles.forEach(t => {
    if (t.asInstanceOf[RadioButton].getText.equals(lang)){
      t.setSelected(true)
    }
  })

  languageChoice.selectedToggleProperty().addListener(new ChangeListener[Toggle]{
    override def changed(observable: ObservableValue[_ <: Toggle], oldValue: Toggle, newValue: Toggle): Unit = {
      val new_lang = newValue.asInstanceOf[RadioButton].getText
      refreshPieChart(new_lang)
    }
  })

  val left = new VBox()
  left.getStyleClass.add("vbox")

  left.getChildren.add(danish)
  left.getChildren.add(dutch)
  left.getChildren.add(english)
  left.getChildren.add(finnish)
  left.getChildren.add(german)
  left.getChildren.add(italian)
  left.getChildren.add(portuguese)
  left.getChildren.add(spanish)


  homebtn.setOnAction( _ => {
    changeSceneToHome()
  })

  def changeSceneToHome(): Unit ={
    Platform.runLater(() => setCenter(new ProgressIndicator()))
    val t: Thread = new Thread(() => {
      val view = new StartView()
      Platform.runLater(()=>getScene.setRoot(view))
    })
    t.start()
  }

  val piechart: PieChart = new PieChart()

  def createPieChart(data: Map[String, Double]): Unit ={
    for (key <- data) {
      val dat: PieChart.Data = new PieChart.Data(key._1 + " " + roundAt(2)(key._2 * 100), key._2 * 100)
      piechart.getData.add(dat)
    }
    piechart.getData.removeIf(d => d.getPieValue == 0.0)
    piechart.setTitle("Share of vowels and consonants")
  }

  def roundAt(p: Int)(n: Double): Double = { val s = math pow (10, p); (math round n * s) / s }


  def refreshPieChart(lang_new : String): Unit = {
    setCenter(new ProgressIndicator())
    changePieChart(lang_new)
  }


  def changePieChart(lang_new:String) : Unit = {
    val thread = new Thread {
      override def run() {
        val data = presenter.vowsAndCons(lang_new)
        val view = new PieChartView(data, lang_new)
        Platform.runLater(()=>getScene.setRoot(view))
      }
    }
    thread.start()
  }

  createPieChart(data)

  top.setAlignment(Pos.CENTER)
  left.setAlignment(Pos.TOP_LEFT)
  bottom.setAlignment(Pos.BOTTOM_CENTER)
  setLeft(left)
  setTop(top)
  setBottom(bottom)
  setCenter(piechart)

}
