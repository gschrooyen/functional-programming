package views

import javafx.application.Platform
import javafx.geometry.Pos
import javafx.scene.control._
import javafx.scene.layout.{AnchorPane, BorderPane, Pane, VBox}
import org.apache.logging.log4j.{LogManager, Logger}
import presenter.NGramsPresenter


class StartView extends BorderPane {

  val presenter = new NGramsPresenter
  val LOGGER: Logger = LogManager.getLogger()

  val title = new Label("Welcome to our NGrams Analyser")
  title.getStyleClass.add("title")
  val welcometext = new Label("Analyse up to eight languages as they are used in a number of texts from the Europarl Corpus")
  val choosetext = new Label("Please choose a type of analysis and a language")
  welcometext.getStyleClass.add("welcome")
  choosetext.getStyleClass.add("welcome")
  val top = new VBox(title, welcometext, choosetext)
  top.setPrefHeight(200)
  top.getStyleClass.add("top")

  val disclaimer = new Label("Made by Glenn and Griet")
  disclaimer.getStyleClass.add("disclaimer")
  val bottom = new VBox(disclaimer)
  bottom.setPrefHeight(50)

  this.getStyleClass.add("pane")

  val languageChoice = new ToggleGroup
  val danish = new RadioButton("Danish")
  danish.setToggleGroup(languageChoice)
  danish.getStyleClass.add("radio")
  val dutch = new RadioButton("Dutch")
  dutch.setToggleGroup(languageChoice)
  dutch.setSelected(true)
  dutch.getStyleClass.add("radio")
  val english = new RadioButton("English")
  english.setToggleGroup(languageChoice)
  english.getStyleClass.add("radio")
  val finnish = new RadioButton("Finnish")
  finnish.setToggleGroup(languageChoice)
  finnish.getStyleClass.add("radio")
  val german = new RadioButton("German")
  german.setToggleGroup(languageChoice)
  german.getStyleClass.add("radio")
  val italian = new RadioButton("Italian")
  italian.setToggleGroup(languageChoice)
  italian.getStyleClass.add("radio")
  val portuguese = new RadioButton("Portuguese")
  portuguese.setToggleGroup(languageChoice)
  portuguese.getStyleClass.add("radio")
  val spanish = new RadioButton("Spanish")
  spanish.setToggleGroup(languageChoice)
  spanish.getStyleClass.add("radio")

  val left = new VBox()
  left.getStyleClass.add("vbox")

  left.getChildren.add(danish)
  left.getChildren.add(dutch)
  left.getChildren.add(english)
  left.getChildren.add(finnish)
  left.getChildren.add(german)
  left.getChildren.add(italian)
  left.getChildren.add(portuguese)
  left.getChildren.add(spanish)

  val center: AnchorPane = new AnchorPane()

  val occurrence = new Label("The occurrence of each letter of the alphabet")
  occurrence.getStyleClass.add("analysis_label")
  val occurbtn = new Button("occurrence")
  occurbtn.setPrefWidth(120.0)
  AnchorPane.setTopAnchor(occurrence, 40.0)
  AnchorPane.setLeftAnchor(occurrence, 200.0)
  AnchorPane.setRightAnchor(occurrence, 400.0)
  AnchorPane.setTopAnchor(occurbtn, 40.0)
  AnchorPane.setRightAnchor(occurbtn, 350.0)

  val begin = new Label("How often a word begins with each letter of the alphabet")
  begin.getStyleClass.add("analysis_label")
  val beginbtn = new Button("begin")
  beginbtn.setPrefWidth(120.0)
  AnchorPane.setTopAnchor(begin, 80.0)
  AnchorPane.setLeftAnchor(begin, 200.0)
  AnchorPane.setRightAnchor(begin, 400.0)
  AnchorPane.setTopAnchor(beginbtn, 80.0)
  AnchorPane.setRightAnchor(beginbtn, 350.0)


  val end = new Label("How often a word ends with each letter of the alphabet")
  end.getStyleClass.add("analysis_label")
  val endbtn = new Button("end")
  endbtn.setPrefWidth(120.0)
  AnchorPane.setTopAnchor(end, 120.0)
  AnchorPane.setLeftAnchor(end, 200.0)
  AnchorPane.setRightAnchor(end, 400.0)
  AnchorPane.setTopAnchor(endbtn, 120.0)
  AnchorPane.setRightAnchor(endbtn, 350.0)

  val vow_con = new Label("The frequency of vowels resp. consonants")
  vow_con.getStyleClass.add("analysis_label")
  val vow_con_btn = new Button("vowCon")
  vow_con_btn.setPrefWidth(120.0)
  AnchorPane.setTopAnchor(vow_con, 160.0)
  AnchorPane.setLeftAnchor(vow_con, 200.0)
  AnchorPane.setRightAnchor(vow_con, 400.0)
  AnchorPane.setTopAnchor(vow_con_btn, 160.0)
  AnchorPane.setRightAnchor(vow_con_btn, 350.0)

  val bigram_begin = new Label("The 25 most frequently occurring bigrams at the start of words")
  bigram_begin.getStyleClass.add("analysis_label")
  val bigrbegin_btn = new Button("bigram_begin")
  bigrbegin_btn.setPrefWidth(120.0)
  AnchorPane.setTopAnchor(bigram_begin, 200.0)
  AnchorPane.setLeftAnchor(bigram_begin, 200.0)
  AnchorPane.setRightAnchor(bigram_begin, 400.0)
  AnchorPane.setTopAnchor(bigrbegin_btn, 200.0)
  AnchorPane.setRightAnchor(bigrbegin_btn, 350.0)

  val bigram_end = new Label("The 25 most frequently occurring bigrams at the end of words")
  bigram_end.getStyleClass.add("analysis_label")
  val bigr_end_btn = new Button("bigram_end")
  bigr_end_btn.setPrefWidth(120.0)
  AnchorPane.setTopAnchor(bigram_end, 240.0)
  AnchorPane.setLeftAnchor(bigram_end, 200.0)
  AnchorPane.setRightAnchor(bigram_end, 400.0)
  AnchorPane.setTopAnchor(bigr_end_btn, 240.0)
  AnchorPane.setRightAnchor(bigr_end_btn, 350.0)

  val bi_tri = new Label("The 25 most frequently occurring bigrams and trigrams")
  bi_tri.getStyleClass.add("analysis_label")
  val bi_tri_btn = new Button("bi_tri")
  bi_tri_btn.setPrefWidth(120.0)
  AnchorPane.setTopAnchor(bi_tri, 280.0)
  AnchorPane.setLeftAnchor(bi_tri, 200.0)
  AnchorPane.setRightAnchor(bi_tri, 400.0)
  AnchorPane.setTopAnchor(bi_tri_btn, 280.0)
  AnchorPane.setRightAnchor(bi_tri_btn, 350.0)

  val skip = new Label("The 25 most frequently occurring skipgrams")
  skip.getStyleClass.add("analysis_label")
  val skip_btn = new Button("skip")
  skip_btn.setPrefWidth(120.0)
  AnchorPane.setTopAnchor(skip, 320.0)
  AnchorPane.setLeftAnchor(skip, 200.0)
  AnchorPane.setRightAnchor(skip, 400.0)
  AnchorPane.setTopAnchor(skip_btn, 320.0)
  AnchorPane.setRightAnchor(skip_btn, 350.0)

  val biskip = new Label("Bigrams corresponding to the 25 most frequently occurring skipgrams")
  biskip.getStyleClass.add("analysis_label")
  val biskip_btn = new Button("bi_skip")
  biskip_btn.setPrefWidth(120.0)
  AnchorPane.setTopAnchor(biskip, 360.0)
  AnchorPane.setLeftAnchor(biskip, 200.0)
  AnchorPane.setRightAnchor(biskip, 400.0)
  AnchorPane.setTopAnchor(biskip_btn, 360.0)
  AnchorPane.setRightAnchor(biskip_btn, 350.0)

  center.getChildren.addAll(occurrence, occurbtn, begin, beginbtn, end, endbtn, vow_con, vow_con_btn, bigram_begin, bigrbegin_btn, bigram_end, bigr_end_btn, bi_tri, bi_tri_btn, skip, skip_btn, biskip, biskip_btn)

  addEventHandlers(center)

  top.setAlignment(Pos.CENTER)
  left.setAlignment(Pos.TOP_LEFT)
  setTop(top)
  setLeft(left)
  setCenter(center)
  setBottom(bottom)
  bottom.setAlignment(Pos.BOTTOM_CENTER)


  def changeViewToPieChart(data: Map[String, Double], lang: String): Unit = {
    val view = new PieChartView(data, lang)
    Platform.runLater(() => getScene.setRoot(view))
  }

  def changeViewToBarGraph(data: Map[String, Double], lang: String, analysis: String): Unit = {
    val view = new BarGraphView(data, lang, analysis, false)
    Platform.runLater(() => getScene.setRoot(view))
  }

  def addEventHandlers(pane: Pane): Unit = {
    pane.getChildren.forEach {
      case pane: Pane =>
        addEventHandlers(pane)
      case button: Button =>
        button.setOnAction(t => {
          val selectedRadioButton: RadioButton = languageChoice.getSelectedToggle.asInstanceOf[RadioButton]
          val lang: String = selectedRadioButton.getText
          val action: String = t.getSource.asInstanceOf[Button].getText
          action.toLowerCase match {
            case "occurrence" =>
              Platform.runLater(() => setCenter(new ProgressIndicator()))
              val t: Thread = new Thread(() => {
                val data = presenter.getOccurrence(lang)
                changeViewToBarGraph(data, lang, "occurrence")
              })
              t.start()
            case "begin" =>
              Platform.runLater(() => setCenter(new ProgressIndicator()))
              val t: Thread = new Thread(() => {
                val data = presenter.getBeginsWith(lang)
                changeViewToBarGraph(data, lang, "begin")
              })
              t.start()
            case "end" =>
              Platform.runLater(() => setCenter(new ProgressIndicator()))
              val t: Thread = new Thread(() => {
                val data = presenter.getEndsWith(lang)
                changeViewToBarGraph(data, lang, "end")
              })
              t.start()
            case "vowcon" =>
              Platform.runLater(() => setCenter(new ProgressIndicator()))
              val t: Thread = new Thread(() => {
                val data: Map[String, Double] = presenter.vowsAndCons(lang)
                changeViewToPieChart(data, lang)
              })
              t.start()
            case "bigram_begin" =>
              Platform.runLater(() => setCenter(new ProgressIndicator()))
              val t: Thread = new Thread(() => {
                val data = presenter.beginsWithBigrams(lang)
                changeViewToBarGraph(data, lang, "bigram_begin")
              })
              t.start()
            case "bigram_end" =>
              Platform.runLater(() => setCenter(new ProgressIndicator()))
              val t: Thread = new Thread(() => {
                val data = presenter.endsWithBigrams(lang)
                changeViewToBarGraph(data, lang, "bigram_end")
              })
              t.start()
            case "bi_tri" =>
              Platform.runLater(() => setCenter(new ProgressIndicator()))
              val t: Thread = new Thread(() => {
                val data = presenter.getBiAndTriOccurrence(lang)
                changeViewToBarGraph(data, lang, "bi_tri")
              })
              t.start()
            case "skip" =>
              Platform.runLater(() => setCenter(new ProgressIndicator()))
              val t: Thread = new Thread(() => {
                val data = presenter.getSkipOccurrence(lang)
                changeViewToBarGraph(data, lang, "skip")
              })
              t.start()
            case "bi_skip" =>
              Platform.runLater(() => setCenter(new ProgressIndicator()))
              val t: Thread = new Thread(() => {
                val data = presenter.getBiSkip(lang)
                changeViewToBarGraph(data, lang, "bi_skip")
              })
              t.start()
            case _ =>
              LOGGER.warn("Could not find analysis falling back to vowels and consonants")
              val data: Map[String, Double] = presenter.vowsAndCons(lang)
              changeViewToPieChart(data, lang)
          }
        })
      case _ =>
    }
  }

}
